from django.urls import path
from . import views as blogViews

app_name = 'blog'

urlpatterns = [
    path('', blogViews.all_blogs, name='all'),
    path('<int:blog_id>/', blogViews.detail, name='detail'),
]
