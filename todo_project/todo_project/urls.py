"""todo_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from todo import views as todoViews

urlpatterns = [
    path('admin/', admin.site.urls),

    # Auth URLS
    path('signup/', todoViews.signupuser, name='signupuser'),
    path('logout/', todoViews.logoutuser, name='logoutuser'),
    path('login/', todoViews.loginuser, name='loginuser'),

    # TODOS URLS
    path('', todoViews.home, name='home'),
    path('create/', todoViews.createtodo, name='createtodo'),
    path('current/', todoViews.currenttodos, name='currenttodos'),
    path('completed/', todoViews.completedtodos, name='completedtodos'),
    path('todo/<int:todo_pk>/', todoViews.viewtodo, name='viewtodo'),
    path('todo/edit/<int:todo_pk>/', todoViews.edittodo, name='edittodo'),
    path('todo/complete/<int:todo_pk>/',
         todoViews.completetodo, name='completetodo'),
    path('todo/undo-complete/<int:todo_pk>/',
         todoViews.undocompletetodo, name='undocompletetodo'),
    path('todo/delete/<int:todo_pk>/',
         todoViews.deletetodo, name='deletetodo'),
]
