# vars

age = 30

name = 'Kostas'

sentence = f'Hello world! My name is {name} and I am {age} years old! (1)'
print(sentence)

print(f'Hello world! My name is {name} and I am {age} years old! (2)')
print('Hello world! My name is {} and I am {} years old! (3)'. format(name, age))

# if statement

if age > 18:
    print("You are older than 18")
else:
    print("You are younger than 18")

youAreOld = True

if youAreOld:
    print("You are old")
else:
    print("You are young")

if name == 'Kostas':
    print("Your name is Kostas")
else:
    print("Your nama is not Kostas")

year = 2020
if 2000 <= year < 2100:
    print('Welcome to the 21st century!')

# functions


def hello(name, age=0):
    print('Hello from function hello(). Hello {}, you are {} years old!'.format(name, age))
    return 'Hello {}'.format(name)


def tripleprint(word):
    print(word*3)


functionsReturn = hello(name, age)
print(functionsReturn)

tripleprint(name)

# list (arrays)

dognames = ["Rachel", "Puggy", "Skilos"]
dognames.insert(0, "Sila")
del(dognames[3])
print(dognames)
print(dognames[2])
print(len(dognames))

# loops

for dogname in dognames:
    print(dogname)

for x in range(1, 5):
    print(x)

while age <= 40:
    print(age)
    age += 1

# dictionaries

lang = {"contact": "Επικοινωνία", "content": "Περιεχόμενο",
        "lang": "Ελληνικά", "dictionary": "Λεξικό", "year": "Έτος"}
print(lang)
del(lang["year"])
lang["month"] = "Μήνας"
print(lang)
print(lang["dictionary"])

# classes


class Dog:
    dogInfo = "Dogs are cool!"

    def __init__(self, name, age, fucolor):
        self.name = name
        self.age = age
        self.fucolor = fucolor

    def bark(self, name=""):
        print('BARK! ' + name)


mydog = Dog("Rachel", 2, "white")
print(mydog.name)
print(mydog.age)
print(mydog.fucolor)

mydog.name = "Puggy"
mydog.age = 1
mydog.bark("Puggy")
print(mydog.name)
print(mydog.age)
